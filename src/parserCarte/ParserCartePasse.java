package parserCarte;

public abstract class ParserCartePasse extends ParserCarte{
	
	public ParserCartePasse(ParserCarte suivant) {
		super(suivant);
	}

	public abstract void parser(String ligne) throws Exception;
	
	
	public abstract boolean saitParser(String ligne);

}
