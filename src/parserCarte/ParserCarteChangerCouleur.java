package parserCarte;

public abstract class ParserCarteChangerCouleur extends ParserCarte{

	public ParserCarteChangerCouleur(ParserCarte suivant) {
		super(suivant);
	}

	public abstract void parser(String ligne) throws Exception;
	
	
	public abstract boolean saitParser(String ligne);

}
