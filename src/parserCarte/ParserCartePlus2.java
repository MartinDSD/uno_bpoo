package parserCarte;

public abstract class ParserCartePlus2 extends ParserCarte{

	public ParserCartePlus2(ParserCarte suivant) {
		super(suivant);
	}

	public abstract void parser(String ligne) throws Exception;
	
	
	public abstract boolean saitParser(String ligne);

}
