package parserCarte;

public abstract class ParserCarte {
		
		private ParserCarte suivant = null;
		
		public ParserCarte(ParserCarte suivant) {
			this.suivant = suivant;
		}

		public void traiter(String ligne) throws Exception {
			if (saitParser(ligne))
				
				parser(ligne);
			else if (aUnSuivant()) 
				
				getSuivant().traiter(ligne);
			else
				throw new ParserManquantException();			
		}

		private ParserCarte getSuivant() {
			return suivant;
		}

		private boolean aUnSuivant() {
			return suivant != null;
		}

		public abstract void parser(String ligne) throws Exception;
		
		
		public abstract boolean saitParser(String ligne);
}
