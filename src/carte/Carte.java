package carte;

import partie.Partie.COULEURS;

public class Carte {
	
	private COULEURS couleur = null;

	public Carte() {
		super();
	}

	public Carte(COULEURS couleur) {
		super();
		this.couleur = couleur;
	}

	public COULEURS getCouleur() {
		return couleur;
	}

	public void setCouleur(COULEURS couleur) {
		this.couleur = couleur;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Carte other = (Carte) obj;
		return couleur == other.couleur;
	}
	
	public void Effet() {
		
	}
	
	
	
}
	
	
	
