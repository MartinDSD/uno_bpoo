package carte;

public class CarteSimple extends Carte{
	private int Valeur = 0;

	public CarteSimple(int valeur) {
		super();
		Valeur = valeur;
	}

	public int getValeur() {
		return Valeur;
	}

	public void setValeur(int valeur) {
		Valeur = valeur;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarteSimple other = (CarteSimple) obj;
		return Valeur == other.Valeur;
	}
	
	public void Effet() {
		
	}
	
	

}
