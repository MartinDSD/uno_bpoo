package joueur;

import java.util.ArrayList;

import carte.Carte;

public class Joueur {
	
	private String nom;
	private boolean estUNO = false;
	private boolean peutJouer = false;
	private ArrayList<Carte> listeCartes = new ArrayList<Carte>();
	
	public Joueur(String nom) {
		super();
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean isEstUNO() {
		return estUNO;
	}

	public void setEstUNO(boolean estUNO) {
		this.estUNO = estUNO;
	}

	public boolean isPeutJouer() {
		return peutJouer;
	}

	public void setPeutJouer(boolean peutJouer) {
		this.peutJouer = peutJouer;
	}

	public ArrayList<Carte> getListeCartes() {
		return listeCartes;
	}

	public void setListeCartes(ArrayList<Carte> listeCartes) {
		this.listeCartes = listeCartes;
	}
	
	public void direUno() {
		
	}
	
	public void piocherCarte() {
		
	}
	
	public void jouerCarte(Carte carte) {
		
	}
	
}
