package fichierCSV;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import parserCarte.ParserCarte;

public class FichierCSV {
	
	public static void lire(String nomFichier, ParserCarte parser) {
		if (nomFichier == null)
			throw new IllegalArgumentException("nomFichierCSV == null !!");
		
		File fichier = new File(nomFichier);
		
		if (! fichier.isFile())
			throw new IllegalArgumentException("Fichier non présent !!");
		
		BufferedReader reader = null;
		String ligne;

		try {
			reader = new BufferedReader(new FileReader(fichier));
				
			while ((ligne = reader.readLine()) != null) {
				if (parser==null)
					System.out.println("Ligne : "+ligne);
				else
					try {
						parser.traiter(ligne);
					}
					catch (ParserCarteManquantException e) {
						System.err.println("Aucun parser n'existe pour la ligne "+ligne);
					}
					catch (Exception e) {
						e.printStackTrace();
					}
			}
			
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
