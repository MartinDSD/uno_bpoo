package parserCarte;

public abstract class ParserCarteSimple extends ParserCarte{
	
	public ParserCarteSimple(ParserCarte suivant) {
		super(suivant);
	}

	public abstract void parser(String ligne) throws Exception;
	
	
	public abstract boolean saitParser(String ligne);
}

