package parserCarte;

public abstract class ParserCartePlus4 extends ParserCarte{

	public ParserCartePlus4(ParserCarte suivant) {
		super(suivant);
	}

	public abstract void parser(String ligne) throws Exception;
	
	
	public abstract boolean saitParser(String ligne);

}
